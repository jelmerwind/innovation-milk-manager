# figure, displaying the data for an individual. This figure consists of two plots.
# In the top plot, the available data for each individual is depicted. The bottom plot
# visualizes the feature extraction
from milkDatabase import query, getLactation,getNumbers
import matplotlib.pyplot as plt
import getFrame
from matplotlib.widgets import Button
import numpy as np
import datetime as datetime
import pandas as pd
import config
import tools
import scipy.signal as sig
import seaborn as sns
sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 3})

class MilkPlot(object):
    ind = 0                     # index of the currently plotted individual in animal_numbers
    frame = []                  # the frame from getFrame
    health = []                 # the health dataframe
    milk = []                   # the milk dataframe
    activity = []               # the activity dataframe
    animal_values = []          # sorted list of values (usually the Indicator from InteractiveTable)
    animal_numbers = []         # list of animal numbers sorted in correspondance to animal_values
    
    varName = ''                # Sort with respect to this variable
    partiName = ''              # name of the participant
    fig = []
    def __init__(self,frame=None,milk=None,health=None,activity=None):
        # initialize
        if frame is None or milk is None:
            frame,milk,health,activity = getFrame.getFrame()
        else:
            self.frame = frame 
            self.milk = milk
            if health is None:
                self.health = getFrame.getHealth()
            else:
                self.health = health
            if activity is None:
                self.activity = getFrame.getActivity()
            else:
                self.activity = activity
        self.fig = plt.figure()
        mng = plt.get_current_fig_manager()
        mng.window.showMaximized()
    def setQuantile(self,partiName,varName,quantileNr=0.5):
        '''Select a farm, sort the animal list in the object and set the
         index. Plot when done.
         partiName - participant code (farm)
         varName   - column name to sort to
         quantileNr - quantile to set the index to'''
        self.partiName = partiName
        self.varName = varName
        partiFrame = self.frame[self.frame['PARTICIPANT_CODE'] == partiName]
        self.animal_numbers = partiFrame.sort_values(varName)['ANIMAL_NUMBER']
        self.animal_values = partiFrame.sort_values(varName)[varName]
        self.ind = int(np.round((len(self.animal_numbers)-1)*quantileNr))
        self.setCalf()
        self.doPlot()        

    def setAnimal(self,animalNr):
        '''Set the object to a given animal or list of animals. Plot when done.'''
        self.animal_numbers = pd.Series(animalNr)
        if animalNr is str:
            N = 0
        else:
            N = len(animalNr)
            
        self.animal_values = pd.Series([0]*N)
        self.ind = 0
        self.setCalf()
        self.doPlot()
    def setCalf(self):
        ''' handle calving data'''
        ind = self.frame.ANIMAL_NUMBER.isin(self.animal_numbers)
        self.calf = self.frame[ind][["CALF_DATE","CALF_PARITY","ANIMAL_NUMBER"]]
        self.calf = self.calf.set_index("ANIMAL_NUMBER")        
    ''' do the plot '''        
    def doPlot(self):
        hasLabels = False                          # keep track if the command plt.legend is necessary
        i = self.ind % len(self.animal_numbers)    # current index in the animal_numbers array
        animalNr = self.animal_numbers.iloc[i]
        timeSum,milkSum,milkCount = tools.sumMilk(self.milk,animalNr)
        
        # prepare figure
        plt.figure(self.fig.number)
        plt.clf()
        # create axis
        axSize = 3 # size of axis compared to buttons
        axRef = plt.subplot2grid(((2*axSize+2),1),(0,0),colspan=2,rowspan=axSize)
        plt.subplot2grid(((2*axSize+2),1),(axSize,0),colspan=2,sharex = axRef,rowspan=axSize)
        # feature plot
        config.features.get(timeSum,milkSum,True,calf=self.calf.loc[animalNr])
        plt.sca(axRef)
        # -------------------------------------------------------------------------
        #           Plot the days in milk indicator
        # -------------------------------------------------------------------------        
        try:
            calfDateNr = float(self.calf.loc[animalNr].CALF_DATE)
            if calfDateNr > 0:        
                calfDate = datetime.datetime.fromtimestamp(calfDateNr)
                axRef.fill_between([calfDate,calfDate + datetime.timedelta(days=305)],[0,0],[70,70],color='w',alpha=1)
                for dayInMilk in np.append(np.arange(0,300,25),305):
                    da = calfDate + datetime.timedelta(days=int(dayInMilk))
                    plt.text(da,65,str(dayInMilk))        
                plt.fill((calfDate,calfDate + datetime.timedelta(days=305)),(70,70))
        except KeyError:
            print("milkPlot::doPlot Parity: KEY_ERROR")        
        
        # -------------------------------------------------------------------------
        #                             ACTIVITY
        # -------------------------------------------------------------------------   
        activityFrame = self.activity[self.activity.ANIMAL_NUMBER==animalNr]       
        if len(activityFrame) > 0:
            INDI = activityFrame.INDICATION
            timeANr = activityFrame.INDICATION_DTM
            timeA = np.array([datetime.datetime.fromtimestamp(nr) for nr in timeANr])
            intervalLogi = np.array((timeANr>np.min(timeSum)) & (timeANr<np.max(timeSum)))
            timeA = timeA[intervalLogi]
            INDI = np.array(INDI[intervalLogi])
            colorNames = ['b'  ,'g'  ,'r'  ,'c'  ,'m'   ,'k'   ,'r', 'y']
            indNames   = ['DAC','ACT','EAT','WAL', 'STU', 'STA', 'LYI', 'PLC']        
            for nameNr in range(len(colorNames)):            
                indTimeA = timeA[INDI == indNames[nameNr]]
                indOnes = np.ones(sum(INDI==indNames[nameNr]))
                if np.any(INDI==indNames[nameNr]):
                    # plot vertical line for an activity notification
                    hasLabels = True
                    indLines = plt.plot_date((indTimeA),(65*indOnes),colorNames[nameNr] + "o",linewidth=4.0,markersize=10)    
                    indLines[0].set_label(indNames[nameNr])        
                    plt.plot_date((indTimeA,indTimeA),(0*indOnes,65*indOnes),colorNames[nameNr] + "-",alpha=0.2)    
        # -------------------------------------------------------------------------
        #                               HEALTH
        # -------------------------------------------------------------------------
        healthFrame = self.health[self.health.ANIMAL_NUMBER == animalNr].dropna()
        healthFrame = healthFrame[healthFrame.DIAGNOSIS_DTM.between(np.min(timeSum),np.max(timeSum))]
        dateGroup = healthFrame.groupby('DIAGNOSIS_DTM')
        colorNames = ['b'  ,'g'  ,'r'  ,'c'  ,'m'   ,'k'   ,'r', 'y']

        legendList = []    
        # loop over vertical lines for health diagnosis dates
        for date,group in dateGroup:
            t = datetime.datetime.fromtimestamp(date)
            diagList = sorted(list(group.DIAGNOSIS_CODE.unique()))
            legendStr = " - ".join(diagList);
            if not legendStr in legendList:
                # plot and add to legend
                hasLabels = True
                legendNr = len(legendList)
                legendNr = np.mod(legendNr,len(colorNames)) 
                legendList=legendList + [legendStr]
                plt.plot_date((t,t),(0,75),colorNames[legendNr] + "^",label=legendStr,markerSize=10)
            else:
                # plot but do not add to legend
                legendNr = legendList.index(legendStr)
                legendNr = np.mod(legendNr,len(colorNames))             
                plt.plot_date((t,t),(0,75),colorNames[legendNr] + "^",markerSize=10)
            plt.plot_date((t,t),(0,75),colorNames[legendNr],alpha=0.2)
            
        # --- finalize top figure            
        if hasLabels:
            leg = plt.legend(frameon=True)
            leg.get_frame().set_facecolor('#FFFFFF')
        titleString = "%s in '%s' (%d/%d) %s:%3.2f" % (animalNr,self.partiName,i,len(self.animal_numbers),
                 self.varName,self.animal_values.iloc[i])
        plt.title(titleString)
        tools.plotDb(timeSum,milkCount, 'b')
        tools.plotDb(timeSum,milkSum, 'k',)

        plt.grid()
        plt.draw()                        

        # -------------------------------------------------------------------------
        #                               BUTTONS
        # -------------------------------------------------------------------------        
        if len(self.animal_numbers) > 1:
            axprev = plt.subplot2grid(((2*axSize+2),2),((2*axSize),0),colspan=1,rowspan =1)
            axnext = plt.subplot2grid(((2*axSize+2),2),((2*axSize),1),colspan=1,rowspan =1)
            self.bnext = Button(axnext, 'Next')
            self.bnext.on_clicked(self.nextClicked)
            self.bprev = Button(axprev, 'Previous')
            self.bprev.on_clicked(self.prevClicked)
        
        plt.tight_layout()
    def nextClicked(self, event=None):
        '''Handle click on the next button'''
        self.ind += 1
        self.doPlot()

    def prevClicked(self, event=None):
        '''Handle click on the previous button'''
        self.ind -= 1
        self.doPlot()
if __name__ == '__main__':
    if not 'frame' in vars():
        frame,milk,health,activity = tools.getData()
    plt.close('all')
    # --- Create a Qt application if this is not a Qt IDE ---
    import sys
    from PyQt4 import QtGui
    makePyQt = QtGui.QApplication.instance() is None
    if makePyQt:
        app = QtGui.QApplication(sys.argv)
    # --- create an instance of MilkPlot
    self = MilkPlot(frame,milk,health,activity)
    # set an interesting list of animals
    self.setAnimal(["NL 708299452", "NL 499992761","DE 1303772927","DE 1303467809","NL 731545371","NL 718004169","NL 696200854","NL 519492608"])

    # wait for the window to be closed and die with diginity
    if makePyQt:    
        sys.exit(app.exec_())
    