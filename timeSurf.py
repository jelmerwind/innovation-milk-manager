import interactiveTable
import milkPlot
import names
import matplotlib.pyplot as plt
import sqlite3
import matplotlib.cm
import pandas as pd
import numpy as np
import datetime
import tools


  
def healthMarkers(health,xAx,yAx,healthNames=None):
    if healthNames is None:
        names.healthNames['Claw']
    diseaseList = ['Zoolzweer','Bevangenheid','Morellaro','Stinkpoot']
    healthFrame = health[health["DIAGNOSIS_CODE"].isin(diseaseList)]
    healthUsed = healthFrame.groupby(["ANIMAL_NUMBER","DIAGNOSIS_DTM"],as_index=False).agg({"DIAGNOSIS_INTENSITY":np.max})

    dateArr = np.array([datetime.date.fromtimestamp(t) for t in healthUsed.DIAGNOSIS_DTM])
    indList  = [yAx.index(n) if n in yAx else -1 for n in  healthUsed.ANIMAL_NUMBER]
    
    indArr   = np.array(indList)
    indUsed  = indArr!=-1
    indUsed  = indUsed & (dateArr >= min(xAx))
    indUsed  = indUsed & (dateArr <= max(xAx))
 
    
    intensity = np.array(healthUsed.DIAGNOSIS_INTENSITY)
    intensityLevels =  np.unique(intensity)
    marker = dict(zip(intensityLevels,['yo','mo','ro','ko']))
    for i in intensityLevels:
        indNow = indUsed & (intensity==i) 
        plt.plot(dateArr[indNow],indArr[indNow] + 0.5,marker[i],markerSize=10)    
    
def timeSurf(milk,participantCode,sortList=None):
    #%%

    mFrame = milk[milk.PARTICIPANT_CODE == participantCode]
    mSum = mFrame.groupby(["ANIMAL_NUMBER","DATE"],as_index=False).sum()
    mSorted = mSum.set_index(['ANIMAL_NUMBER','DATE'])
    timeArr = tools.getTimeArr(mFrame.DATE)

    timeFrame = pd.DataFrame(index=timeArr , columns = None)
    for animalNr,animalDf in mSorted.groupby(level=0):
         animalCol = animalDf.loc[animalNr]['MILK']
         timeFrame[animalNr] = animalCol  
    timeFrame = timeFrame.transpose()

    if not sortList:
        sortVal = list(timeFrame.count(axis=1))
    else:
        sortVal = [sortList.index(n) if n in sortList else -1 for n in timeFrame.index]
        
    outFrame = timeFrame.iloc[np.argsort(sortVal)]        
        
    outArr = np.array(outFrame) 
    maskedData = np.ma.masked_where(np.isnan(outArr), outArr)
    dateList = [datetime.date.fromtimestamp(t) for t in timeArr]
    nameInd = np.arange(outArr.shape[0])

    plt.clf()
    plt.pcolormesh(dateList,nameInd,maskedData,cmap=plt.get_cmap('jet'))
    plt.title(participantCode)
    plt.colorbar()
    yAx = list(outFrame.index)
    xAx = dateList
    #%%
    return (xAx,yAx)
if __name__ == '__main__':
    if not "milk" in vars():
        frame,milk,health,activity = tools.getData()

    plt.close('all')       
    xAx,yAx = timeSurf(milk,'Paris')    
    healthMarkers(health,xAx,yAx)
