import datetime
import numpy as np
import matplotlib.pyplot as plt
import getFrame
import pandas as pd
import pandas.io.sql as pd_sql
import sqlite3 as sql
import config
import os
import scipy.signal as sig
# ----------------------------------------------------------------------------
#                             Plotting
# ----------------------------------------------------------------------------
def plotDb(tStamp,y,_color=None,**kwargs):
    tPy = np.array([datetime.datetime.fromtimestamp(t) for t in tStamp])        
    if _color is not None:
        plt.plot(tPy,y,_color,**kwargs)
    else:
        plt.plot(tPy,y,**kwargs)
def plotSig(tStamp,y,sig,color=None,**kwargs)        :
    if color is not None:
        kwargs["color"] = color    
    std = np.sqrt(sig)
    tPy = np.array([datetime.datetime.fromtimestamp(t) for t in tStamp])      
    plt.plot(tPy,y,**kwargs)      
    plt.gca().fill_between(tPy,y-std,y+std,**kwargs,alpha=0.2)    
# ----------------------------------------------------------------------------
#                    Filtering dayly milk
# ----------------------------------------------------------------------------

# get a list of unix timestamps corresponding with time 0:00 on each day in the list.
# correct for daylight saving
def getOut(yVec,N=11,thr=5):
    ''' 
    Determine outliers using a causal median filter
    yVec - vector (of milk production)
    N    - number of data points to use in filter [11.]
    thr  - threshold in kilos of milk [5.]
    '''
    mVec = sig.medfilt(yVec,N)
    nHalf = round((N-1)/2)
    pVec = np.concatenate((yVec[0:(N-1)],                  # replace first points with original: no outliers
                           mVec[nHalf:-nHalf]))    # make median filter CAUSAL
    isOut = abs(pVec-yVec) > thr      
    return isOut
# get array with dateTimes without any gaps. Take into account daylight saving
def getTimeArr(dateSeries):
    seconds = 3600*24

    minStamp = min(dateSeries)
    maxStamp = max(dateSeries)
    nDay = int(round((maxStamp - minStamp)/seconds))
    minDate = datetime.date.fromtimestamp(minStamp)
    dateList = [minDate + datetime.timedelta(days = i) for i in range(nDay+1)]
    timeArr = np.array([datetime.datetime.combine(d, datetime.datetime.min.time()).timestamp() for d in dateList])
    return timeArr
# get array of milk data with NAN for missing days    
def getMilkArr(timeVec,timeSum,milkSum):
    timeList = list(timeVec)
    indT = [timeList.index(t) for t in timeSum]
    yVec = np.nan *np.array(np.ones(timeVec.shape))
    yVec[indT] = milkSum    
    return yVec
# select the period with the most consecutive milkings. This is used to
# find one lactation
def select(timeSum,milkSum,milkCount=None):           
    rangeUsed = selectRange(timeSum)
    timeSum = timeSum.iloc[rangeUsed]
    milkSum = milkSum.iloc[rangeUsed]   
    if milkCount is None:
        return timeSum,milkSum
    else:
        milkCount = milkCount.iloc[rangeUsed]
        return timeSum,milkSum,milkCount    
    
# return the indices of the mail lactation
def selectRange(timeSum):           
    c = np.diff(timeSum)<config.maxSkip*3600*24
    iszero = np.concatenate(([0], c.view(np.int8), [0]))
    absdiff = np.abs(np.diff(iszero))
    ranges = np.where(absdiff == 1)[0].reshape(-1, 2)        
    if len(ranges)>0:
        rangeInd = ranges[np.argmax(np.diff(ranges))]
        rangeUsed = np.arange(rangeInd[0],rangeInd[1])
    else:
        rangeUsed = np.zeros(timeSum.shape,dtype="bool")
    return rangeUsed

# get daily milk production. Do not change missing days
def sumMilk(milk,animal_number=None):
    if animal_number is not None:
        milk = milk[milk.ANIMAL_NUMBER==animal_number]
    milkUsed = milk[['MILK','DATE']].groupby("DATE")
    timeSum = milkUsed.DATE.nth(0)
    if config.DO_MEAN:
        milkSum = milkUsed.MILK.mean() *2
    else:
        milkSum = milkUsed.MILK.sum()    
        
    milkCount = milkUsed.MILK.count()
    return timeSum,milkSum,milkCount
# OBSOLETE get daily milk production. Add NAN for missing days
def dayMilk2(milk,animal_number):
    milkUsed = milk[milk.ANIMAL_NUMBER == animal_number].groupby("DATE")
    time = milkUsed.DATE.nth(0)
    timeVec = getTimeArr(time)
    y0 = milkUsed.MILK.sum()
    timeList = list(timeVec)
    indT = [timeList.index(t) for t in time]
    yVec = np.nan *np.array(np.ones(timeVec.shape))
    yVec[indT] = y0;    
    return timeVec,yVec
    

# sort individuals based on the intensity of their claw disorder. Make sure
# data outside the milking interval is excluded    
def clawSorting(health,milk, participantCode):
    # first and last day of lactation
    def milkLim(milk,participantCode):
        d = milk[milk.PARTICIPANT_CODE==participantCode].DATE
        return d.min(),d.max()
    minDate,maxDate = milkLim(milk,participantCode)
    diseaseList = ['Zoolzweer','Bevangenheid','Morellaro','Stinkpoot']
    useHealth = health[health["DIAGNOSIS_CODE"].isin(diseaseList)]
    useHealth = useHealth[useHealth.DIAGNOSIS_DTM.between(minDate,maxDate)]
    useHealth = useHealth[useHealth.PARTICIPANT_CODE == participantCode]
    intensityFrame = useHealth.groupby("ANIMAL_NUMBER").agg({"DIAGNOSIS_INTENSITY":np.max})
    sortFrame = intensityFrame.sort_values("DIAGNOSIS_INTENSITY")
    sortList = list(sortFrame.index)
    return sortList
    
# ----------------------------------------------------------------------------
#                    Database interaction
# ----------------------------------------------------------------------------    
# call getFrame to compute the overview of the database. Save the result in an SQLite database
def computeFrame():
    fName = config.FILE_NAME + config.PARTICIPANT_CODE + ".sqlite"
    if os.path.isfile(fName):
        os.remove(fName)    
    frame,milk,health,activity = getFrame.getFrame(config.PARTICIPANT_CODE )
    con = sql.connect(fName)        
    frame.to_sql('frame',con)
    milk.to_sql('milk',con)         # note : unnecessary 10 seconds
    health.to_sql('health',con)
    activity.to_sql('activity',con)    
    return frame,milk,health,activity
# get the frame. Load it from file if it is available and config.AUTO_RECOMPUTE==False
# Otherwise, recompute the database and store it for future reference
def getData():
    if config.AUTO_RECOMPUTE:
        frame,milk,health,activity = computeFrame()
    else:
        fName = config.FILE_NAME + config.PARTICIPANT_CODE + ".sqlite"
        con = sql.connect(fName)        
        cursor = con.cursor()
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        a = cursor.fetchall()
        if len(a)==0:
            con.close()            
            userAnswer = input("Database is empty. Recompute? [y]")
            recomp = False
            if len(userAnswer)==0:
                recomp = True
            elif userAnswer[0]=='y':
                recomp = True
            if recomp:
                frame,milk,health,activity = computeFrame()
        else:
            frame = pd.read_sql("select * from frame",con)
            milk = pd.read_sql("select * from milk",con)
            health = pd.read_sql("select * from health",con)
            activity = pd.read_sql("select * from activity",con)
            con.close()
    return frame,milk,health,activity

    
    
    
    
    
    
    
    
    
    
    