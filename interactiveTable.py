# Display a table that summarizes all the information in FRAME for each
# PARTICIPANT_CODE (farm). The values are ordered from low to high using quantiles.
# Clicking on a cell launches a MilkPlot for that quantile, with working buttons
# for previous and next.
#
# Note that this table is PyQt such that it will not work in a browser
from numbers import Number
import milkPlot
from PyQt4 import QtCore, QtGui
from matplotlib.pyplot import get_cmap
import numpy as np
Qt = QtCore.Qt
import pandas as pd
import tools
import getFrame
# The table itself. #todo: refactor the code such that there is no Icellselected and
# PCellselected, just an internal representation that gets changed around
class InteractiveTable(QtGui.QWidget):
    _frame = None              # frame (the global)
    _table = None              # the TableView (defined below)
    _colSelect = None          # the QCombobox
    _vLayout = None            # the QVBoxLayout
    _plot = None               # the instance of MilkPlot
    _tButton = None            # transpose button
    _isIndicatorView = False   # toggles with the Transpose button
    def __init__(self,frame=None,milk=None,health=None,activity=None):
        super( InteractiveTable,self).__init__(None)    
        # initialize graphics
        self._frame = frame
        self._colSelect = QtGui.QComboBox()            
        self._tButton = QtGui.QPushButton("Transpose")
        self._tButton.setMaximumWidth(380)
        self._tButton.pressed.connect(self.transpose)

        self._vLayout = QtGui.QVBoxLayout()
        self._hLayout = QtGui.QHBoxLayout()
        self._hLayout.addWidget(self._tButton)
        self._hLayout.addWidget(self._colSelect)
        self._vLayout.addLayout(self._hLayout)
        self.participantView()
        self.setLayout(self._vLayout)
        self.showMaximized()            
        self._plot = milkPlot.MilkPlot(frame,milk,health,activity)    
    # callback to the transpose button, switches indicatorView
    # in indicatorView, the listbox selects an indicator such as VAR_PRODUCTION
    # in participantView, the listbox selects a participant (farm) such as Paris
    def transpose(self):
        self._isIndicatorView = not self._isIndicatorView
        if self._isIndicatorView:
             self.indicatorView()
        else:
             self.participantView()
    # callback to cell-selected when in indicator view             
    def onICellSelected(self,tup):
        (name,quant),participant = tup
        self._plot.setQuantile(participant,name,quant)    
    # prepare indicator view
    def indicatorView(self):
        self._colSelect.clear()
        excludeSet = {'ANIMAL_NUMBER','PARTICIPANT_CODE'}      
        nameList = [n for n in self._frame.columns if n not in excludeSet]
        for colName in nameList:
            self._colSelect.addItem(colName)    

        self.setIndicator(nameList[0])
        self._colSelect.currentIndexChanged.connect(
                     lambda : self.setIndicator(self._colSelect.currentText())
            )
    # callback to selecting from the combobox when indicatorMode is true
    def setParticipant(self,colName):
        group = self._frame[self._frame["PARTICIPANT_CODE" ]==colName]
        quant = group.quantile([0,0.05,0.15,0.25,0.5,0.75,0.85,0.95,1]).transpose()            
        self.setTable(quant)
        self._table.cellSelected.connect(self.onPCellSelected)          
    # callback to cell selected in participant mode (not indicator mode)        
    def onPCellSelected(self,tup):
        quant,name = tup        
        participant = self._colSelect.currentText()
        self._plot.setQuantile(participant,name,quant)            
    # prepare participant view
    def participantView(self):
        self._colSelect.clear()
        nameList = self._frame.PARTICIPANT_CODE.unique()                     
        for colName in nameList:
            self._colSelect.addItem(colName)    
        self._colSelect.currentIndexChanged.connect(
                     lambda : self.setParticipant(self._colSelect.currentText())            
                                                     )                                                        
        self.setParticipant(nameList[0])                                             
    # callback to the listbox when in indicatorMode == False                                                    )  
    def setIndicator(self,colName):
        group = self._frame[[colName,'PARTICIPANT_CODE']].groupby('PARTICIPANT_CODE')
        quant = group.quantile([0,0.05,0.15,0.25,0.5,0.75,0.85,0.95,1]).unstack()        
        self.setTable(quant)                     
        self._table.cellSelected.connect(self.onICellSelected)   
    # set the table to the dataframe quant        
    def setTable(self,quant):  
        if self._table:
            self._vLayout.removeWidget(self._table)
            self._table.deleteLater()
        self._table = TableView(quant)
        self._vLayout.addWidget(self._table)
       
# PyQt tableView wich emits a signal when a cell is clicked
class TableView(QtGui.QTableView):
    cellSelected = QtCore.pyqtSignal(tuple)
    def __init__(self,data, parent=None):
        QtGui.QTableView.__init__(self,parent)
        model = TableModel(data)
        self.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)
        self.clicked.connect(self.onClicked)        
        self.setModel(model)
    def onClicked(self,index):
        model = self.model()
        cellName = model.getCellName(index)          
        self.cellSelected.emit(cellName)
# PyQt tableModel with contextual coloring along the rows        
class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, data, parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self._data = data
        # -------------- create colormap --------------------------

        self._colorFrame = self.getColorFrame(data)
    def getColorFrame(self,data):
#        group = self._frame[self._frame["PARTICIPANT_CODE" ]=="Paris"]
#        data = group.quantile([0,0.05,0.15,0.25,0.5,0.75,0.85,0.95,1]).transpose()        
        
        f = lambda x : 155 * x + 100
        numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
        cmap = get_cmap('jet')        
        colorDict = dict()        
        for rowNr in range(len(data)):
            s = data.iloc[rowNr]
            colorList = [QtGui.QColor('White')]*len(s)
            sDrop = s.dropna()
            if str(s.dtypes) in numerics and len(sDrop) > 0:
                minVal = min(sDrop)
                maxVal = max(sDrop)
                if minVal != maxVal:
                    for i in range(len(s)):
                        val = s.iloc[i]
                        if not np.isnan(val):
                            quant = (val-minVal)/(maxVal-minVal)
                            col = cmap(int(255*quant))
                            colorList[i] = QtGui.QColor(f(col[0]),f(col[1]),f(col[2]))
            colorDict[rowNr] = colorList  
        outFrame = pd.DataFrame(colorDict).transpose()            
        return outFrame
    def getCellName(self,index):
        colName = self._data.columns[index.column()]
        rowName = self._data.index[index.row()]
        return (colName,rowName)
    def rowCount(self, parent=None):
        return len(self._data.values)

    def columnCount(self, parent=None):
        return self._data.columns.size

    def data(self, index, role=Qt.DisplayRole):
        if index.isValid():
            if role == Qt.DisplayRole:  
                cellVal = self._data.iloc[index.row()].iloc[index.column()];
                if isinstance(cellVal,Number):
                    return '%3.2f' % cellVal
                else:
                    return str(cellVal)
            if role == Qt.BackgroundColorRole:
                try:
                    a = self._colorFrame
                    color = self._colorFrame.iloc[index.row()].iloc[index.column()];
                except:
                    print(index.row(),index.column())
                return color
        return None
    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                 # Note that columns may have names which are lists, due to
                 # unstack operatos. Plot the last one (inner index)
                colName =  self._data.columns[section]
                if type(colName) == tuple:
                    colName = colName[-1]
#               colList = self._data.columns[section]
#                colName = '';
#                for l in colList:
#                    if isinstance(l,Number):
#                        colName = colName + '%3.2f' % l + ' '
#                    else:
#                        colName = colName + str(l)
            if orientation == Qt.Vertical:
                colName = self._data.index[section]
            return str(colName)
        return QtCore.QAbstractTableModel.headerData(self, section, orientation, role)   
if __name__ == '__main__':
    if not 'frame' in vars():
        frame,milk,health,activity = tools.getData()
    import sys
    from PyQt4 import QtGui
    makePyQt = QtGui.QApplication.instance() is None
    if makePyQt:
        app = QtGui.QApplication(sys.argv)

    self = InteractiveTable(frame,milk,health,activity)    
    self.onPCellSelected((0.95,'CALF_DATE'))
    if makePyQt:    
        sys.exit(app.exec_())