# Methods to detect dips in production using the difference between two kalman filters
# tl;dr: check the comments in get() to get started
import matplotlib.pyplot as plt
import tools
import seaborn as sns
import pandas as pd
import numpy as np
import scipy.signal as sig
import datetime
days = 3600*24
riseTime = 20
avgTime = 10    
maxSkip = 14
minDays = 20
refStamp = datetime.datetime.strptime("06 May 16", "%d %b %y").timestamp()

def babyKalman(timeVec,milkVec,curveVec,sigVec,doPlot=True):
    ''' Kalman filter which is intended follow normal lactation curves, but go straight at dips.
         link: https://en.wikipedia.org/wiki/Kalman_filter
         input timeVec  - unix timestamps for all consecutive days in a lactation
               milkVec  - daily milk production for the days corresponding to timeVec, NaN if no data is available
               curveVec - nominal lactation curve for the days in timeVec
               sigVec   - variance for the days in timeVec '''   
    
    sigWeight = 5e0                                     # for tuning: 1 factor that impacts the variance but not the mean
    R0 = 5e2                                            # for tuning: ratio between obeservation noise and process noise (square root)
    R = R0*sigWeight                                    # observation noise
    upMulti = 1                                         # unused: adapt faster when production is more than expected. Activate by setting to 0.2
    
    # the state vector is a 2 vector [daily milk production, derivative of daily milk prodiction],
    # But the derivative is currently unused because F[1,1] == 0    
    # See wikipedia for more information about the following matrices
    P = np.matrix('''1. 0.;
                     0. 0.1''')*5e1*sigWeight              # initial uncertainty
    Q = np.matrix('''1. 0.;
                     0. 0.1''')*sigWeight/R0               # the covariance of the process noise: multiply by sigVec
    F = np.matrix('''1. 1.;
                     0. 0.''')                             # process model
    H = np.matrix('''1. 0.''')                             # observation model

    # compute the starting value of the Kalman filter
    useVec = milkVec[riseTime:avgTime] 
    useVec = useVec[~np.isnan(useVec)]
    if len(useVec)==0:
        useVec = 0
    x = np.matrix([np.median(useVec),0]).T                 # initial state
    I = np.matrix(np.eye(F.shape[0]))                      # identity matrix    

    xVec = np.zeros(milkVec.shape)                         # output vector
    sVec = P[0,0]*np.ones(milkVec.shape)                   # variance vector
    relVec = milkVec - curveVec                            # input vector: difference between wilmink curve and daily milk
    isOut = tools.getOut(relVec,thr=7) | np.isnan(relVec)  # identify outliers
    
    # loop over production days
    for i in range(len(relVec)):
        # wikipedia's UPDATE step        
        if not isOut[i]:
            y = np.matrix(relVec[i]).T - H * x             # difference between expected measurement and actual measurement
            rMulti = upMulti if y > 0 else 1               # currently unused: adapt faster if the production is higher than expected
            S = H * P * H.T + R*rMulti                     # residual convariance
            K = P * H.T * S.I                              # Kalman gain
            x = x + K*y                                    # update state vector
            P = (I - K*H)*P                                # update state covariance
   
        # wikipedia's PREDICT step
        x = F*x                                            # predict the next state
        P = F*P*F.T + Q*sigVec[i]                          # predict the next covariance 
        xVec[i] = x.item(0) + curveVec[i]                  # save the current state 
        sVec[i] = P[0,0]                                   # save the current covariance for milk production 
    if doPlot:
        tools.plotSig(timeVec,xVec,sVec,'b')               # plot the state and covariance vector (blue area) 
        tools.plotDb(timeVec[isOut],milkVec[isOut],'ro')   # plot the outliers (red circles)
    return xVec,sVec            
def kalman(timeVec,milkVec,curveVec,sigVec,doPlot=True):
    ''' Kalman filter for the short time average the lactation curve 
         link: https://en.wikipedia.org/wiki/Kalman_filter
         input timeVec  - unix timestamps for all consecutive days in a lactation
               milkVec  - daily milk production for the days corresponding to timeVec, NaN if no data is available
               curveVec - nominal lactation curve for the days in timeVec
               sigVec   - variance for the days in timeVec '''   
    sigWeight = 1e-3    # for convenience: 1 factor that impacts the variance but not the mean
    R = 5e4*sigWeight   # observation noise
    upMulti = 1         # unused: adapt faster when production is more than expected. Activate by setting to 0.2
    
    P = np.matrix('''1. 0.;
                     0. 0.1''')*1e5*sigWeight # initial uncertainty
    Q = np.matrix('''1. 0.;
                     0. 0.1''')*1e2*sigWeight  # the covariance of the process noise: multiply by sigVec
    F = np.matrix('''1. 1.;
                     0. 1.''')   # process model
    H = np.matrix('''1. 0.''')

    # compute the starting value of the Kalman filter
    useVec = milkVec[riseTime:avgTime] 
    useVec = useVec[~np.isnan(useVec)]
    if len(useVec)==0:
        useVec = 0
    x = np.matrix([np.median(useVec),0]).T                 # initial state
    I = np.matrix(np.eye(F.shape[0]))                      # identity matrix    

    xVec = np.zeros(milkVec.shape)                         # output vector
    sVec = P[0,0]*np.ones(milkVec.shape)                   # variance vector
    relVec = milkVec - curveVec                            # input vector: difference between wilmink curve and daily milk
    isOut = tools.getOut(relVec,thr=7) | np.isnan(relVec)  # identify outliers
    
    # loop over production days
    for i in range(len(relVec)):
        # wikipedia's UPDATE step        
        if not isOut[i]:
            y = np.matrix(relVec[i]).T - H * x             # difference between expected measurement and actual measurement
            rMulti = upMulti if y > 0 else 1               # currently unused: adapt faster if the production is higher than expected
            S = H * P * H.T + R*rMulti                     # residual convariance
            K = P * H.T * S.I                              # Kalman gain
            x = x + K*y                                    # update state vector
            P = (I - K*H)*P                                # update state covariance
   
        # wikipedia's PREDICT step
        x = F*x                                            # predict the next state
        P = F*P*F.T + Q*sigVec[i]                          # predict the next covariance 
        xVec[i] = x.item(0) + curveVec[i]                  # save the current state 
        sVec[i] = P[0,0]                                   # save the current covariance for milk production 
    if doPlot:
        tools.plotSig(timeVec,xVec,sVec,'r')               # plot the state and covariance vector (red area) 
        tools.plotDb(timeVec[isOut],milkVec[isOut],'ro')   # plot the outliers (red circles)
    return xVec,sVec
def paritySelect(timeSum,milkSum,calf):
    ''' select the days corresponding to the single, longest lactation.
         Take into account the fact that only one calving is known.
        
         input  timeSum:      timestamps corresponding to milkSum, possibly non-consecutive
                milkSum:      daily milk
                calf:         dataframe containing CALF_PARITY and CALF_DATE.
        
         output timeVec:      vector of consecutive timestamps of the selected lactation
                milkVec:      daily milk corresponding to timeVec (NaN if no data is available)
                daysInMilkVec:inferred number of days since calving
                useParity:    1 for first calf, 2 for second calf, etc         '''
        
    refParity = float( calf.CALF_PARITY )
    refDate = float( calf.CALF_DATE )
    yearRelative = np.floor( (timeSum-refDate)/356/days )   # for each milking day: in which lactation is it
    yearDelta = yearRelative.value_counts().idxmax()        # select lactation with the most data
    useDate = refDate + yearDelta*356*days                  # inferred calving date
    useParity = refParity + yearDelta                       # inferred parity
    
    daysInMilkSum = (timeSum - useDate)/days                # days in milk corresponding to timeSum
    selector = (daysInMilkSum>0) & (daysInMilkSum < 356)    # bools indicating if a day in milk is selected
    timeSelect = timeSum[selector]
    milkSelect = milkSum[selector]        
    
    timeVec = tools.getTimeArr(timeSelect)                      # create an array of consecutive days for this lactation
    milkVec = tools.getMilkArr(timeVec,timeSelect,milkSelect)   # get the corresponding milk
    daysInMilkVec = (timeVec-useDate)/days                      # corresponding days in milk
    return timeVec,milkVec,daysInMilkVec,useParity
 
def makeWilmink(daysInMilkVec,milkVec):
    '''  Compute the nominal lactation curve based on the days in milk and the milkVec.
        
         input: daysInMilkVec : inferred number of days since calving
                milkVec :       milk production
         output: wilVec :       nominal lactation curve
                 sigVec :       variance relative to lactation curve        '''
    wil = lambda b0,b1,b2,k,x : b0 + b1*x + b2*np.exp(k*x) # parametric version of the curve  
    interval = (daysInMilkVec > 30) & (daysInMilkVec < 60) & ~np.isnan(milkVec)  # interval used for selection
    if any(interval):
        # mode 0: use the median production of the interval to set the maximum of the curve
        interMedian =np.median(milkVec[interval])
        wilVec = wil(interMedian + 2,-0.03,-interMedian/2 - 3,-0.07,daysInMilkVec)
    else:
        interval2 = (np.arange(daysInMilkVec.shape[0]) < 15) & ~np.isnan(milkVec)
        if any(interval2):
            # mode 1: use the first 15 points whatever they are 
            interMedian = np.median(milkVec[interval2])
            wilVec0 = wil(28,-0.04,-17,-0.07,daysInMilkVec)
            interwilVec = np.median(wilVec0[interval2])
            wilVec = wilVec0 + interMedian - interwilVec
        else:
            # mode 2: no data available
            print("No points for bootstrapping : %s" % animalNr)
            wilVec = wil(28,-0.04,-17,-0.07,daysInMilkVec)
    sigVec = 30*np.exp(-0.1*daysInMilkVec) + 30000*np.exp(-0.07*(365-daysInMilkVec)) + 1
    return wilVec,sigVec    
    

def get(timeSum,milkSum,doPlot=False,calf=None,full=False):
    '''  timeVec,distVec,countVec = get(timeSum,milkSum,doPlot=False,calf=None)    
         compute a feature that detects dips in the milk production and optionally visualize
         the result. Output:
             timeVec:  np.array of time instances (unix timestamps)
             distVec:  np.array of feature values
             countVec: np.array of integers: 1 if milk data was available, 0 otherwise  '''  
        
    timeVec,milkVec,daysInMilkVec,useParity = paritySelect(timeSum,milkSum,calf) # select 1 lactation
    wilVec,sigVec = makeWilmink(daysInMilkVec,milkVec)                           # extract the nominal curve
    mu0,sig0 = babyKalman(timeVec,milkVec,wilVec,sigVec,doPlot)                  # lactation curve for no dips
    muA,sigA = kalman(timeVec,milkVec,wilVec,sigVec,doPlot)                      # local average
    if doPlot:
        tools.plotDb(timeSum,milkSum,'r.',alpha=0.5)                             # plot the input (small red dots) 
        tools.plotDb(timeVec,milkVec,'k.')                                       # plot the selected lactation (black dots)
        tools.plotDb(timeVec,wilVec,'g', label="parity = %d" % useParity )       # plot the nominal curve

    distVec = (mu0-muA)/np.sqrt(0.5*(sigA+sig0))                                 # compute the distance
    countVec = (~np.isnan(milkVec)).astype('int')                                # 1 if milk data is available for that day, 0 otherwise
    if doPlot:
        plt.gca().twinx()                                                        # another axis on the right
        tools.plotDb(timeVec,distVec)                                            # plot the distance, pale blue
    if not full:       
        return timeVec,distVec,countVec
    else:
        return timeVec,mu0,sig0,muA,sigA       
def getDict(timeSum,milkSum,doPlot=False,calf=None):
    ''' For the day set by refStamp, get the difference between expected and actual
         production in absolute terms and relative to the estimated variance.
        This method is called by getFrame to extract FEAT_DIFF and FEAT_REL
        
        input  : timeSum  - non-consecutive days for which milk information is known
                 milkSum  - milk data corresponding to timeSum
                
        output : Dict containing absolue and relative difference
                '''
                
    timeVec,mu0,sig0,muA,sigA = get(timeSum,milkSum,doPlot,calf,full=True)
    sigS = 1/2*(sig0+sigA)
    useVec = timeVec==refStamp
    if any(useVec):
        return {"DIFF":  muA[useVec] - mu0[useVec],
                "REL" : (muA[useVec] - mu0[useVec])/sigS[useVec] }
    else:
        return {"DIFF": 0,
                "REL" : 0}
if __name__ == "__main__":

    if not 'frame' in vars():   
        frame,milk,health,activity = tools.getData()
    plt.clf()
    animalNr ="NL 708299452" #"NL 880305855" #"NL 880305831" #"NL 718004082" #"NL 484989239" #"NL 693809278" #"NL 499992761" # #  # "DE 1304442902" 
    calf = frame[frame.ANIMAL_NUMBER==animalNr][["CALF_DATE","CALF_PARITY","ANIMAL_NUMBER"]]
    timeSum,milkSum,countSum =  tools.sumMilk(milk,animalNr)   
    get(timeSum,milkSum,calf=calf,doPlot = True)
    doPlot = True