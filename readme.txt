tl;dr: interactiveTable gives an overview of the data. Use the spyder IDE

milkManager is a toolkit to visualize activity, health and production data
for dairy cattle. It is written in python, and it makes extensive use of pandas and pyQt.

This version of MilkManager is set op to work only with the Spyder IDE (the default IDE of the Anaconda distro)
In spyder, go to Tools->Preferences->IPython Console go to the tap Graphics and set Backend to Automatic
