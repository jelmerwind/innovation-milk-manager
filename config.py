#-------------------------------------------------------------------------------
#                           configuration file.
#-------------------------------------------------------------------------------
import os.path as op                                # just for this file
import wilmink as features                      # config.features.get(..) runs the current feature extractor.
DATA_DIR = "resources/"                               # folder containing the SQLite database
DB_FILE = DATA_DIR + "merged1.db"                   # SQLite file
PARITY_FILE = DATA_DIR + "KalfenPariteitUBNs.xls"   # parity file
AUTO_RECOMPUTE = False                              # automatically recompute frame whenever tools.getData
                                                    #     is called (True is inefficient)
PARTICIPANT_CODE = ""                          # e.g: "Paris", participant used to compute the frame. 
                                                    #     "" means all participants
FILE_NAME = op.split(op.splitext(DB_FILE)[0])[1] # select file name from DB_FILE

DO_MEAN = True                                      # for tools.sumMilk: compute the daily milk production as 2x the mean milking
maxSkip = 14                                        # number of days to skip in tools.select
