# File containing translations from codes to readable names. (To be cleaned up)
import pandas as pd
import numpy as np
NAMES_FILE = "resources/IRIS_records.xls"
irisDiagnosis = pd.read_excel(NAMES_FILE,"Diagnosis")  # diagnosis codes
irisTreatment = pd.read_excel( NAMES_FILE,"Treatment")  # treatment codes

# code names for each participant
partiDict = {1731:"Venice",
             14481:"Tokyo",
             31024:"Dublin",
             32205:"Las Vegas",
             39356:"Berlin",
             49597:"New York",
             176659:"London",
             262226:"Rio",
             376318:"Paris"}
partiInv = {v: k for k, v in partiDict.items()}    # reverse partiDict

# below is the REAL list of transition diseases. Do no delete.
#healthNrs = {"Transition": [22,96,126,127,62,63,64,65,66,67,68,125,79,81],"Claw":[24,26,27,28,30,102,200,201,202]}

# Diagnosis codes are distinguished into two categories: Transition and Claw
healthNrs = {"Transition": [125,79],"Claw":[24,26,27,28,30,102,200,201,202]}
ignoreNrs = [199,990,999,998,997]             # codes of not really diagnosis things, e.g.: "look again next time"
def _getIris():
    iris = {"DIAGNOSIS_CODE":dict(),"DIAGNOSIS_TYPE":dict(),"DIAGNOSIS_AREA":dict(),
            "TREATMENT_TYPE":dict(),"TREATMENT_AREA":dict() }
    for colName in iris.keys():
        try:
            col = irisDiagnosis[colName]
        except KeyError:
            col = irisTreatment[colName]
        col = col[col.notnull()]
        for s in col:
            (nrStr,nameStr) = s.split(",",1)
            if nrStr.isdigit():
                nr = int(nrStr)
                iris[colName][nr] = nameStr
    iris["PARTICIPANT_CODE"] = partiDict
    return iris
def _getHealthNames():
    healthNames = dict()
    for keyName in healthNrs.keys():
        healthNames[keyName] =  [iris["DIAGNOSIS_CODE"][i] for i in healthNrs[keyName]] 
    return healthNames
def _getAllNames():
    allNames = []
    for nameList in healthNames.values():
        allNames = allNames + nameList
    return allNames
    
iris = _getIris()
healthNames = _getHealthNames()
ignoreNames =  [iris["DIAGNOSIS_CODE"][i] for i in ignoreNrs] 
#transitionNumbers = [22,96,126,127,62,63,64,65,66,67,68,125,79,81]
allNames = _getAllNames()

