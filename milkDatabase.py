# create the database that is used by getFrame to create Pandas dataframes
# Note that this step is not really necessary in Pandas because it supports a
# lot of input formats anyway. This file was developed before the codebase was
# switched to Pandas
import csv
import sqlite3
import datetime
import matplotlib.pyplot as plt
import xlrd
import numpy as np
mode = 'create'
dbFile = 'resources/test0.db' if mode is 'test' else 'resources/merged1.db'
#milkFiles = ['resources/Emm_melkingen_test']
#dbFile = 'resources/Emm_melkingen.db'
healthFile = 'resources/alle diergezondheidsdata.xlsx'
milkFiles = ['resources/Emm_melkingen_2014','resources/Emm_melkingen_2015','resources/Emm_melkingen_2016']
activityFile = 'resources/Activiteitsdata.xlsx'
clawFile = 'resources/Klauwgezondheidsdata.xlsx'
hideZeroTimes = True

def column(matrix, i):
    return np.array([row[i] for row in matrix])
   
def createMilk():
    conn = sqlite3.connect(dbFile)         
    c = conn.cursor()
    c.execute('CREATE TABLE IF NOT EXISTS milk(PARTICIPANT_CODE INT, ANIMAL_NUMBER STRING, DATE INT, DATE_TIME INT, MILK real )')
    c.execute('DELETE FROM milk')
            
    for inFile in milkFiles:
        print('transferring ' + inFile)
        with open(inFile, newline='') as csvfile:
            rows = csv.reader(csvfile, delimiter='\t', quotechar='|')
                 
    
            next(rows)
            for row in rows:
                timeVar = ''.join(['0'] * (4-len(row[3])) + [row[3]]);
                if not hideZeroTimes or row[3]!="0":
                    tDate =  datetime.datetime.strptime(row[2],'%Y-%m-%d').timestamp()
                    tFull = datetime.datetime.strptime(row[2] +" " +  timeVar,'%Y-%m-%d %H%M').timestamp()
                    
                    c.execute('INSERT INTO milk(PARTICIPANT_CODE, ANIMAL_NUMBER, DATE, DATE_TIME, MILK) VALUES (?,?,?,?,?)',
                              (row[0],row[1],tDate,tFull,row[5]))
    conn.commit()
    c.close()
    conn.close()
def createClaw():
    conn = sqlite3.connect(dbFile)         
    c = conn.cursor()
    createQuery =("CREATE TABLE IF NOT EXISTS claw( " 
    "ANIMAL_NUMBER STRING,ANIMAL_NUMBER_SHORT INT,ANIMAL_NAME STRING,"
    "PARTICIPANT_CODE INT,PAR_ID_OBSERVER INT,DIAGNOSIS_DTM INT," 
    "DIAGNOSIS_TYPE INT,DIAGNOSIS_CODE INT,DIAGNOSIS_AREA INT,"  
    "DIAGNOSIS_INTENSITY INT,PAR_ID_EXECUTOR INT,EXECUTOR_ROLE_TYPE STRING,"  
    "TREATMENT_BEGIN_DTM INT,TREATMENT_END_DTM INT,TREATMENT_TYPE INT," 
    "BARN_TYPE STRING,FLOOR_TYPE STRING,PASTURE_TYPE STRING,DIAGNOSIS_TREATMENT_DTM INT)")   

    c.execute(createQuery)
    c.execute('DELETE FROM claw')    

    book = xlrd.open_workbook(clawFile)
    sheet = book.sheet_by_index(0)
    keys = [sheet.cell(0, col_index).value for col_index in range(sheet.ncols)]
    nRows = sheet.nrows
    dispPerc = 0
    for row_index in range(1, nRows):
        newDispPerc = int(100*row_index/nRows)
        if newDispPerc == dispPerc + 10:
            dispPerc = newDispPerc
            print("preparing claw " + str(dispPerc))
        row = sheet.row(row_index)
        cells = [cell.value 
            if cell.ctype is not 3
            else int(datetime.datetime(*xlrd.xldate_as_tuple(cell.value,
                                     book.datemode)).timestamp())
            for cell in row]    
        insertQuery ="insert into claw(" + ",".join(keys) + ") values (" +"?,"*(len(keys)-1)  +  "?)"
        c.execute(insertQuery,tuple(cells))
   
    conn.commit()
    c.close()
    conn.close()       
def createHealth():
    conn = sqlite3.connect(dbFile)         
    c = conn.cursor()
    createQuery =("CREATE TABLE IF NOT EXISTS health( " 
    "ANIMAL_NUMBER STRING,ANIMAL_NUMBER_SHORT INT,ANIMAL_NAME STRING,"
    "PARTICIPANT_CODE INT,PAR_ID_OBSERVER INT,DIAGNOSIS_DTM INT," 
    "DIAGNOSIS_TYPE INT,DIAGNOSIS_CODE INT,DIAGNOSIS_AREA INT,"  
    "DIAGNOSIS_INTENSITY INT,PAR_ID_EXECUTOR INT,EXECUTOR_ROLE_TYPE STRING,"  
    "TREATMENT_BEGIN_DTM INT,TREATMENT_END_DTM INT,TREATMENT_TYPE INT," 
    "BARN_TYPE STRING,FLOOR_TYPE STRING,PASTURE_TYPE STRING,DIAGNOSIS_TREATMENT_DTM INT)")   

    c.execute(createQuery)
    c.execute('DELETE FROM health')    

    book = xlrd.open_workbook(healthFile)
    sheet = book.sheet_by_index(0)
    keys = [sheet.cell(0, col_index).value for col_index in range(sheet.ncols)]
    nRows = sheet.nrows
    dispPerc = 0
    for row_index in range(1, nRows):
        newDispPerc = int(100*row_index/nRows)
        if newDispPerc == dispPerc + 10:
            dispPerc = newDispPerc
            print("preparing health " + str(dispPerc))
        row = sheet.row(row_index)
        cells = [cell.value 
            if cell.ctype is not 3
            else int(datetime.datetime(*xlrd.xldate_as_tuple(cell.value,
                                     book.datemode)).timestamp())
            for cell in row]    
        insertQuery ="insert into health(" + ",".join(keys) + ") values (" +"?,"*(len(keys)-1)  +  "?)"
        c.execute(insertQuery,tuple(cells))
   
    conn.commit()
    c.close()
    conn.close()      
def createActivity():
    print("activity ...")
    conn = sqlite3.connect(dbFile)         
    c = conn.cursor()
    createQuery =("CREATE TABLE IF NOT EXISTS activity( " 
    "ANIMAL_NUMBER STRING,PARTICIPANT_CODE INT,INDICATION_TYPE STRING,"
    "INDICATION STRING ,INDICATION_DTM INT,INDICATION_END_DTM INT,"
    "INDICATION_CERTAINTY STRING)")   

    c.execute(createQuery)
    c.execute('DELETE FROM activity')    
        
    book = xlrd.open_workbook(activityFile)
    sheet = book.sheet_by_index(0)
    keys = [sheet.cell(0, col_index).value for col_index in range(sheet.ncols)]
    for row_index in range(1,  sheet.nrows):
        row = sheet.row(row_index)
        cells = [cell.value 
             if cell.ctype is not 3
             else int(datetime.datetime(*xlrd.xldate_as_tuple(cell.value,
                                      book.datemode)).timestamp())
             for cell in row]    
        insertQuery ="insert into activity(" + ",".join(keys) + ") values (" +"?,"*(len(keys)-1)  +  "?)"
        c.execute(insertQuery,tuple(cells))        
    conn.commit()
    c.close()
    conn.close()    
def createInfo():

    conn = sqlite3.connect(dbFile)         
    c = conn.cursor()  
    try:
        createQuery =("CREATE TABLE IF NOT EXISTS meta(ANIMAL_NUMBER STRING,"
        "PARTICIPANT_CODE STRING, N_MILK INT, N_ACTIVITY INT, N_HEALTH INT,"
        "N_CLAW INT)")
        c.execute(createQuery)
        c.execute('DELETE FROM meta')    
        c.execute( '''insert into meta(ANIMAL_NUMBER,PARTICIPANT_CODE,N_MILK,N_ACTIVITY,N_CLAW,N_HEALTH)
        select ANIMAL_NUMBER,PARTICIPANT_CODE, sum(N_MILK),sum(N_ACTIVITY),sum(N_CLAW),sum(N_HEALTH) from (
        select ANIMAL_NUMBER,PARTICIPANT_CODE, COUNT(ANIMAL_NUMBER) as N_MILK,0 as N_ACTIVITY, 0 as N_CLAW, 0 as N_HEALTH from milk group by ANIMAL_NUMBER
        UNION
        select ANIMAL_NUMBER,PARTICIPANT_CODE,0,COUNT(ANIMAL_NUMBER),0,0 from activity group by ANIMAL_NUMBER
        UNION
        select ANIMAL_NUMBER,PARTICIPANT_CODE, 0,0,COUNT(ANIMAL_NUMBER),0 from claw group by ANIMAL_NUMBER
        UNION
        select ANIMAL_NUMBER,PARTICIPANT_CODE, 0,0,0,COUNT(ANIMAL_NUMBER) from health group by ANIMAL_NUMBER
        )
        group by ANIMAL_NUMBER
         ''')
        conn.commit()        
    finally:

        c.close()
        conn.close()               
            
        
def query(queryString):
    conn = sqlite3.connect(dbFile)
    c = conn.cursor()
    c.execute(queryString)
    rows = c.fetchall()
    c.close()
    conn.close()
    if len(rows)==0:
        return ()
    else:
        s = len(rows[0])
        a = tuple([column(rows,i) for i in range(s)])
        return a   
def getLactation(numberStr):
    rows = query("SELECT DATE,SUM(MILK) FROM milk WHERE ANIMAL_NUMBER='"+numberStr+"' GROUP BY DATE ORDER BY DATE")    
    return rows   
def getNumbers():
    return  query("SELECT DISTINCT ANIMAL_NUMBER FROM milk")[0]
    
if __name__ == "__main__":
    
    if mode == "create":
       createMilk()
       createHealth()
       createClaw()
       createActivity()
       createInfo()
    elif mode == "test":      
        createInfo()
    elif mode == "plot":
        numbers = getNumbers()
        for i in range(3):
            plt.figure()
            numberUsed = numbers[i][0]
            (time,y0) = getLactation(numberUsed)
            plt.plot_date(time,y0,'b-')   
            plt.title(numberUsed)
