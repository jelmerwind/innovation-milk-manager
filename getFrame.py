#-------------------------------------------------------------------------------
#   Create a data frame containing statistics of each animal, then summarize 
#   for each participant
#-------------------------------------------------------------------------------
import pandas as pd
import sqlite3
import numpy as np
import datetime as datetime
import names
import config
import tools
import sqlite3 as sql
def toDate(tArr):
    return np.array([datetime.datetime.fromtimestamp(t) for t in tArr])
def column(matrix, i):
    return np.array([row[i] for row in matrix])    
class Database:
    def __init__(self,dbFile):
        self.conn = sqlite3.connect(dbFile)         
        self.c = self.conn.cursor()
    def __enter__(self):
        return self        
    def __exit__(self, exc_type, exc_value, traceback):
        self.conn.commit()
        self.c.close()
        self.conn.close()                
    def execute(self,queryString, vars = ()):
        self.c.execute(queryString,vars)                
    def query(self,queryString, vars = ()):
        self.c.execute(queryString, vars)
        rows = self.c.fetchall()
        if len(rows)==0:
            return ()
        else:
            return tuple([column(rows,i) for i in range( len(rows[0]))])
def getParity():
    '''Get the birthdate of the cow, the birth date of the most recent calf
       and the calf number.
       input:   none
       output:  parity : pandas dataframe containing information about the most recent calf
    '''
    parity = pd.read_excel(config.PARITY_FILE,"KalfenPari")
    parity.columns = ["PARTICIPANT_CODE","ANIMAL_NUMBER", "BIRTH_DATE","GENDER", "PARITY","CALF_DATE"]
    

    parity.CALF_DATE = [datetime.datetime.timestamp(d) for d in parity.CALF_DATE]
    parity.BIRTH_DATE = [datetime.datetime.timestamp(d) for d in parity.BIRTH_DATE]
    parity = parity.replace(names.iris)

    return parity
def getHealth():
    '''Get all the health data. Translate everything using names.py
       input:   None
       ouput:   health :  pandas dataframe containing diagnoses
    '''
    con = sqlite3.connect(config.DB_FILE)        
    df = pd.read_sql_query("SELECT * FROM health ",con)
    con.close()   
       
    dfP = df[df.PARTICIPANT_CODE.isin(names.iris['PARTICIPANT_CODE'])]
    dfP = dfP.replace({'(null)':float('nan')})
    health = dfP.replace(names.iris)
    return health            
    
def getActivity():
    '''Get all the activity data'''
    con = sqlite3.connect(config.DB_FILE)        
    df = pd.read_sql_query("SELECT * FROM activity ",con)
    con.close()   
       
    dfP = df[df.PARTICIPANT_CODE.isin(names.iris['PARTICIPANT_CODE'])]
    dfQ = dfP.replace({"PARTICIPANT_CODE":names.iris["PARTICIPANT_CODE"]})
    return dfQ   

def getFrame(participant_code=''):
        ''' get the "frame" dataframe. It contains a row for each individual
            and columns linking information about milk, health, activity and parity
            
            input  : participant_code. Participant code (e.g. 'Paris') or "" for all farms
            output : frame     - dataframe constructed in this method
                     milk      - milk dataframe from getMilk (to avoid reading from hdd twice)
                     health    - health dataframe from getHealth (to avoid reading from hdd twice) 
                     activity  - activity dataframe from getActivity (to avoid reading from hdd twice)
        '''
        con = sqlite3.connect(config.DB_FILE)       
        if len(participant_code) == 0:
            print("loading entire database")
            sqlMilk = pd.read_sql_query("SELECT ANIMAL_NUMBER,PARTICIPANT_CODE,MILK,DATE,DATE_TIME from milk",con)
        else:
            print("loading participant %s" % participant_code)
            participantNr = names.partiInv[participant_code]
            sqlMilk = pd.read_sql_query("SELECT ANIMAL_NUMBER,PARTICIPANT_CODE,MILK,DATE,DATE_TIME from milk where PARTICIPANT_CODE = ?"
                                        ,con,params=[participantNr])
        con.close()        
        parityGroup = getParity().groupby("ANIMAL_NUMBER")                      # parity frame
        milk = sqlMilk.replace({'PARTICIPANT_CODE':names.partiDict})            # milk frame
        health = getHealth()                                                    # health frame
        activity = getActivity()                                                # activity frame
        # group
        animals = sqlMilk.groupby(['ANIMAL_NUMBER','PARTICIPANT_CODE'],as_index= False)
        healthGroup = health.groupby('ANIMAL_NUMBER') 
        activityGroup = activity.groupby('ANIMAL_NUMBER')

        # prepare the columns of the dataframe
        N= len(animals.groups)
        print("analyzing " + str(N) + " cows ")        
        nHealth = np.zeros(N)
        nActivity = np.zeros(N)
        nOtherHealth = np.zeros(N)
        animalNrs = np.zeros(N,dtype=object)
        participantCodes = np.zeros(N,dtype =int)        
        avgMilk = np.zeros(N)
        varMilk = np.zeros(N)       
        avgCount = np.zeros(N)
        featureDict = dict()
        calfDate = np.zeros(N,dtype=float)     
        calfParity = np.zeros(N,dtype=int)
        
        # names.py defines illness categories and the names associated with them.
        # The code below prepares scores for all named illnesses and the maximum
        # within each category
        healthScores = dict()
        daysInMilk = np.zeros(N)
        for cat in names.healthNames.keys():
            healthScores[cat] = dict()
            for name in names.healthNames[cat]:
                healthScores[cat][name] = np.zeros(N)
            healthScores[cat]["Maximum"] = np.zeros(N) 
            
        #----------------------------------------------------------------------            
        #                   Loop over the animals
        #----------------------------------------------------------------------            
        i = 0        
        for (animalNr,participantCode),animal in animals:
            if (i%100)==0:           
                print("%d / %d" % (i,N))
            timeSum,milkSum,milkCount = tools.sumMilk(animal)
            animalNrs[i] = animalNr
            participantCodes[i] = participantCode
            try:
                animalParity = parityGroup.get_group(animalNr)
                calfDateNow = float(animalParity.CALF_DATE)
                calfDate[i] = calfDateNow                    
                calfParity[i] = animalParity.PARITY
            except KeyError:
                pass
            
            # config.py defines the feature file. It must have a method getDict which returns
            # a dict of scalars. This file is called for every animal and the output is added to the frame
            calf = pd.Series( {"CALF_PARITY":calfParity[i],"CALF_DATE":calfDate[i]})
            featureDictNow = config.features.getDict(timeSum,milkSum,calf = calf)
            if i==0:
                featureDict = {name:np.zeros(N) for name in featureDictNow.keys()}
            for name in featureDictNow.keys():
                featureDict[name][i] = featureDictNow[name]
            avgMilk[i] = milkSum.mean()
            varMilk[i] = milkSum.var()
            avgCount[i] = milkCount.mean()
            daysInMilk[i] = milkSum.count()
            
            # ---  add activity data if available
            try:
                activityDF = activityGroup.get_group(animalNr)
                activityDF = activityDF[activityDF.INDICATION_DTM.between(min(timeSum),max(timeSum))]
                nActivity[i] = len(activityDF)
            except KeyError:
                # apparently there is not activity data for this individual
                pass                
            
            # --- add the health data
            try:
                healthDF = healthGroup.get_group(animalNr)
                healthDF = healthDF[healthDF.DIAGNOSIS_DTM.between(min(timeSum),max(timeSum))]
                healthDF = healthDF[healthDF.DIAGNOSIS_CODE != '(null)']   
                healthDF = healthDF[~healthDF.DIAGNOSIS_CODE.isin(names.ignoreNames)]
                nOtherHealth[i] = np.sum(~healthDF.DIAGNOSIS_CODE.isin(names.allNames))

                nHealth[i] = healthDF.DIAGNOSIS_DTM.nunique()
                if nHealth[i] > 0:
                    # loop over categories: Health, Claw
                    for cat in names.healthNames.keys():
                        # loop over illnesses
                        maxIntensityPerCat = 0
                        for diagnosis in names.healthNames[cat]:
                            nameHealth = healthDF[healthDF.DIAGNOSIS_CODE == diagnosis]
                            # diagnosis intenisty 4 is "not applicable", replace it with 1
                            nameHealth = nameHealth.replace({"DIAGNOSIS_INTENSITY":{4:1}})
                            if len(nameHealth) > 0:
                                maxIntensity = nameHealth.DIAGNOSIS_INTENSITY.dropna().max()
                                healthScores[cat][diagnosis][i] = maxIntensity
                                maxIntensityPerCat = np.max([maxIntensity,maxIntensityPerCat])
                            healthScores[cat]["Maximum"][i] = maxIntensityPerCat
                inSeries = healthDF.DIAGNOSIS_INTENSITY.dropna()

            except KeyError:
                pass      
            i+= 1
        print("done.")
        #----------------------------------------------------------------------            
        #                   Finish up
        #----------------------------------------------------------------------              
        frame = pd.DataFrame( { 'ANIMAL_NUMBER':animalNrs,
                                'PARTICIPANT_CODE':participantCodes,
                                'DAYS_IN_MILK':daysInMilk,
                                'MEAN_PRODUCTION': avgMilk,
                                'VAR_PRODUCTION': varMilk,
                                'MEAN_MILKINGS': avgCount,
                                'N_HEALTH':nHealth,
                                'N_OTHER_HEALTH':nOtherHealth,
                                'N_ACTIVITY':nActivity,
                                'CALF_DATE': calfDate,
                                'CALF_PARITY': calfParity})

        # the health categories and illnesses are defined in names.py
        # add them to frame
        for cat in healthScores.keys():
            for diagnosis in healthScores[cat].keys():
                colName = cat + ": " + diagnosis
                frame[colName] = healthScores[cat][diagnosis]

        # features are defined in config.py (import x as features). Add them to the frame                
        for name in featureDict.keys():
            colName = "FEAT_" + name
            frame[colName] = featureDict[name]
        frame = frame.replace({'PARTICIPANT_CODE':names.partiDict})  
        return frame,milk,health,activity


if __name__ == "__main__":
    import config
    participant_code = config.PARTICIPANT_CODE   
    frame,milk,health,activity = tools.computeFrame()